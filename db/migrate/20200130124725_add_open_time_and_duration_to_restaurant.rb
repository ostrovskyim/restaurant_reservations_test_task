class AddOpenTimeAndDurationToRestaurant < ActiveRecord::Migration[6.0]
  def change
    add_column :restaurants, :open_time, :string
    add_column :restaurants, :open_duration, :integer
  end
end
