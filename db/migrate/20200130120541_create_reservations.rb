class CreateReservations < ActiveRecord::Migration[6.0]
  def change
    create_table :reservations do |t|
      t.integer :user_id
      t.integer :table_id
      t.datetime :start_datetime
      t.datetime :finish_datetime

      t.timestamps
    end
  end
end
