class Restaurant < ApplicationRecord
  has_many :tables

  validates :title, :open_time, :open_duration, presence: true
end