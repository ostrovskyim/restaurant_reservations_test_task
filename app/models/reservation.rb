class Reservation < ApplicationRecord
  belongs_to :table
  belongs_to :user

  validates :start_datetime, :finish_datetime, presence: true
  validate :isnt_oversected_by_others
  validate :isnt_conflicted_with_restaurant_opening_time

  private

  def isnt_oversected_by_others
    existing_reservations = table.reservations.where('(start_datetime BETWEEN :start AND :finish) || (finish_datetime BETWEEN :start AND :finish)', { start: start_datetime, finish: finish_datetime })
    if existing_reservations.any?
      errors.add(:base, 'Your reservation cannot be created because table are already booked for this time')
    end
  end

  def isnt_conflicted_with_restaurant_opening_time
    open_time = Time.parse(table.restaurant.open_time)
    close_time = open_time + table.restaurant.open_duration.hours
    unless (start_datetime >= open_time) && (finish_datetime <= close_time)
      errors.add(:base, 'Reservation should finish before restaurant is closed')
    end
  end
end
